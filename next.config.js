const { parsed: localEnv } = require("dotenv").config();

const webpack = require("webpack");
const apiKey = JSON.stringify("3cffe4e09be589fcfef3d3c6b4a68db7");

module.exports = {
  webpack: (config) => {
    const env = { API_KEY: "3cffe4e09be589fcfef3d3c6b4a68db7" };
    config.plugins.push(new webpack.DefinePlugin(env));

    // Add ESM support for .mjs files in webpack 4
    config.module.rules.push({
      test: /\.json$/,
      include: /node_modules/,
      type: "javascript/auto",
      loader: "json-loader",
    });

    return config;
  },
};
